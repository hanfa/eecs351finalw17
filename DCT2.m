clc,clear;
tiff = imread('clock.tiff'); % import image
imwrite(tiff, 'clock.jpeg'); % convert to jpeg
jpeg = imread('clock.jpeg');

A= 255.*ones(128,128);
A(63:79,64:80) = 0;
imwrite(A,'watermark.jpeg');
