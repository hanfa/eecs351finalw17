clc,clear;
RGB = imread('girl.tiff'); % import image

I = rgb2gray(RGB); % convert to gray scale
imwrite(I,'girl_gray.tiff');
I1 = imread('girl_gray.tiff');
imwrite(I1, 'girl_gray.jpeg')


% imshow(I); colorbar;
% I2 = im2double(I); % convert uint8 to double
% set quantization matrix
Q = [ 16   11   10   16   24   40   51   61;
      12   12   14   19   26   58   60   55;
      14   13   16   24   40   57   69   56;
      14   17   22   29   51   87   80   62;
      18   22   37   56   68  109  103   77;
      24   35   55   64   81  104  113   92;
      49   64   78   87  103  121  120  101;
      72   92   95   98  112  100  103   99];
   
% z = cell(32,32);
% for i = 1:8:256 % row
%     for j = 1:8:256 % column
%         block = I2(i:i+7, j:j+7); % partition into 8-by-8
%         z{i,j} = dct(block); % perform DCT
%         zf = round(z{i,j}./m).*m;
%     end
% end
% 
% c = mat2cell(I2,[8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8],...
%     [8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8]); % partitioned into 8-by-8 blocks
% 
% y = cell(32,32);
% z = cell(32,32);
% for i = 1:32
%     for j = 1:32
%         y{i,j} = dct(c{i,j}); % performing DCT to each 8-by-8 block
%         z{i,j} = round(y{i,j}./m);
%     end
% end

