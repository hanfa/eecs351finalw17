# This is a modify#

# EECS351 Proposal: Invisible Watermark #

This shows the Proposal details:
### Problem Statement ###
Watermark is a faint design made in paper or electronic pictures. A traditional watermark should be visible and clear
enough for reader to see when held against the light. However, the traditional watermarks will affect the sense of
vision and destroy the harmony of the original image if they are too obvious. With traditional watermark, the
content of watermark will be exposed to the public even if the publisher only want a designated group of people to
see it. Hence, we aim to implement an algorithm that can not only generate an invisible watermark but also extract it
from a watermarked picture.

### Dataset ###

The database we are using is from University of Southern California - Signal and Image Processing Institute,
namely, the USC-SIPI Image Database. �Here is the link http://sipi.usc.edu/database/database.php.
“The USC-SIPI image database is a collection of digitized images. It is maintained primarily to support research in image processing, image analysis, and machine vision”.
The database is composed of four different categories of pictures. They are textures, aerials, miscellaneous and sequences. Images in each category are of various sizes such as 256x256 pixels, 512x512 pixels, or 1024x1024
pixels. All images are 8 bits/pixel for black and white images, 24 bits/pixel for color images.

###  Programming environments ###

Matlab

### Method ###

In this project, we are going to use discrete cosine transformation (DCT) and chaos mapping method to do
watermarking and encryption. We first use a grayscale image as the host image, cut the image into each 8x8 pieces,
and do DCT. Then we will use a chaos mapping function to permute the black-white watermark. After that we will
embed the permuted watermark into the middle frequency of each 8x8 pieces, replace the original DCT, and then
transform the host image into spatial domain. To decrypt it and get the invisible watermark, just do the inverse
process.

Here are different ways of encryptions:
*Permute a 128x128 black-white image by a secrete key with length 128 bit and do XOR with original
watermark row by row.
*Use chaos mapping.

### Quantitative Measure ###

To measure the quality of the invisible watermarking, we will test the signal-to-noise ratio (SNR) of retrieved
watermark using different host images, under different JPEG compression ratios, under different rotation angles of
watermarked host image. Moreover, we will apply different kinds of attacks to the watermarked host image,
(including image smearing, shear cutting, stretching, rotating and JPEG compression) and see whether we can
retrieve watermark with good quality.
