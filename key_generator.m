function key = key_generator()

length_dataset = [11 13 17 19 23 29];
position_dataset = [3 5 7 11 13 15];

length = randi([1,6]);
position = randi([1,6]);

while(position_dataset(position)>=length_dataset(length) || (length == 3 && position == 5) || (length == 2 && position == 4)|| (length == 1 && position == 3))
    position = randi([1,6]);
end

for i = 1 : length_dataset(length)
    state(i) = randi([0,1]);
end

key.length = length_dataset(length);
key.position = position_dataset(position);
key.initial_state = state;