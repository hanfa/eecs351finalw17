%% File description: this is a way to encrypt a black and white image using m-sequence. The key to encrypt it is
%  the initial state of the registers
%  the position of where to do the XOR
%  the length of the registers

function param= encryption(original_watermark,filename)
watermark = imread(original_watermark); 
watermark = rgb2gray(watermark);

%% Parameters
param = key_generator();
register_length = param.length;
position = param.position;
reg = param.initial_state;

% register_length = 17;
% position = 7;
% reg = [1 1 0 1 0 0 1 0 1 0 0 1 0 1 1 0 1];

%% resize the image to 128x128
watermark = imresize(watermark, [128,128]);

[height, width] = size(watermark);

%% Change the grey scale to black and white
for i = 1 : height
    for j = 1 : width
        if(watermark(i,j) < 200)
            watermark(i,j) = 1;
        else
            watermark(i,j) = 0;
        end
    end
end

%% Begin to incrpyt
delay = 1;

data_array = zeros(1,height * width); % scrambled result will be stored in the array first

for i = 1 : height
    for j = 1 : width
        temp_reg = xor(reg(register_length),reg(position));
        for k = register_length : -1 : 1
            if(k == 1)
                reg(k) = temp_reg;
            else
                reg(k) = reg(k-1);
            end
        end
        data_array(delay) = xor(watermark(i,j),temp_reg);
        delay = delay + 1;
    end
end

%% Change into new watermark
new_row = 0;
new_column = 0;
new_watermark = zeros(128,128);
for i = 1 : length(data_array)
    if(mod(i-1,128) == 0)
        new_row = new_row + 1;
        new_column = 1;
    end
    if(data_array(i) == 1)
        new_watermark(new_row, new_column) = 0;
    else
        new_watermark(new_row, new_column) = 255;
    end
    new_column = new_column + 1;
end

imwrite(new_watermark, filename);
