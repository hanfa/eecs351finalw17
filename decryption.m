function decryption(param,encrpted,filename)
watermark = imread(encrpted); 

%% Parameters
register_length = param.length;
position = param.position;
reg = param.initial_state;
%% Attack the encrypted image

% % Some signal is missing during transition
% watermark(20,9) = ~watermark(20,9);
% watermark(79,28) = ~watermark(79,28);
% 
% % Rotate the image
% watermark = watermark';



%% begin to decrypt
[height, width] = size(watermark);

delay = 1;

data_array = zeros(1,height * width); % scrambled result will be stored in the array first

for i = 1 : height
    for j = 1 : width
        temp_reg = xor(reg(register_length),reg(position));
        for k = register_length : -1 : 1
            if(k == 1)
                reg(k) = temp_reg;
            else
                reg(k) = reg(k-1);
            end
        end
        data_array(delay) = xor(watermark(i,j),temp_reg);
        delay = delay + 1;
    end
end

%% Change into new watermark
new_row = 0;
new_column = 0;
new_watermark = zeros(128,128);
for i = 1 : length(data_array)
    if(mod(i-1,128) == 0)
        new_row = new_row + 1;
        new_column = 1;
    end
    if(data_array(i) == 1)
        new_watermark(new_row, new_column) = 255;
    else
        new_watermark(new_row, new_column) = 0;
    end
    new_column = new_column + 1;
end

imwrite(new_watermark, filename);