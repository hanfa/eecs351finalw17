
% clc,clear;
%% File description: This file is to add watermark. After add watermark, the host image will be compressed into new host image
%%                   with different compression ratio
%%                   After that


%%%%%%%%%%%%%%%%%%% Watermark Embedding %%%%%%%%%%%%%%%%%%%%%%%%


Q8 = [ 16   11   10   16   24   40   51   61;
      12   12   14   19   26   58   60   55;
      14   13   16   24   40   57   69   56;
      14   17   22   29   51   87   80   62;
      18   22   37   56   68  109  103   77;
      24   35   55   64   81  104  113   92;
      49   64   78   87  103  121  120  101;
      72   92   95   98  112  100  103   99];
  
Q = Q8(2:5,2:5);  
Delta = 0.01;
Tau = 1.8*Q8;

Iwater = imread('1.png'); % import image, type unit8: 0 & 255
IDoubleWater = im2double(Iwater); % leave 0 and 1

Ihost = imread('aerial.tiff');
IDoubleHost = double(Ihost);

block_c = cell(1,1024);
c = cell(1,1024);
index = 1;
for i = 1:8:256
    for j = 1:8:256
        block_c{index} = dct2(IDoubleHost(i:i+7, j:j+7));
        index = index + 1;
    end
end
for index = 1:1024
    c{index} = block_c{index}(2:5,2:5);
end     

w = cell(1,1024);
index = 1;
for i = 1:4:128
    for j = 1:4:128
    w{index} = IDoubleWater(i:i+3,j:j+3);
    index = index + 1;
    end
end

new_c = cell(1,1024);
new_block_c = cell(1,1024);

for i =1:1024
    new_c{i} = zeros(4,4); 
    new_block_c{i} = block_c{i};
end

%% Calculate tau new
Tau_new = zeros(1,16);
a = 1;
for i = 3 : 6
    for j = 3 : 6
        Tau_new(a) = Tau(i,j);
        a = a + 1;
    end
end

%% Add watermark 
m = 1; % row of watermarking index
n = 1; % column of watermarking index
for l = 2:1024 % index of block
    for j = 1:16 % left starting point of 4*4
            if (c{l-1}(j) <= c{l}(j) && w{l}(j) == 1)
                new_c{l}(j) = c{l}(j) + Delta;
            elseif (c{l-1}(j) > c{l}(j) && w{l}(j) == 1)
                if (c{l-1}(j)+Delta-c{l}(j) < Tau_new(j))
                    new_c{l}(j) = c{l-1}(j) + Delta;
                else new_c{l}(j) = c{l}(j);
                end
            elseif (c{l-1}(j) <= c{l}(j) && w{l}(j) == 0)
                if (c{l}(j)-(c{l-1}(j)/Q(j)-1)*Q(j) < Tau_new(j))
                    new_c{l}(j) = (c{l-1}(j)/Q(j)-1)*Q(j) + Delta;
                else
                    new_c{l}(j) = c{l}(j);
                end 
            elseif (c{l-1}(j) > c{l}(j) && w{l}(j) == 0)
                if (c{l-1}(j)/Q(j) == c{l}(j)/Q(j))
                    new_c{l}(j) = (c{l-1}{j}/Q(j)-1)*Q(j) + Delta;
                else
                    new_c{l}(j) = c{l}(j);
                end
            end
    end
end

for l = 1:1024 
    new_block_c{l}(2:5,2:5) = new_c{l};
end

%% Using JPEG compression, the ratio is compression ratio. 

ratio = 0.04; % Quality: 94% quality


EmbHost = zeros(256,256);
index = 1;
for i = 1:8:256
    for j = 1:8:256
                EmbHost(i:i+7,j:j+7) = idct2(round(new_block_c{index}/(ratio*Q8)) * ratio * Q8) ;
                index = index +1;
    end
end

imwrite(uint8(EmbHost),'embedded_host.jpg');

%%%%%%%%%%%%%%%%%%% Watermark Extracting %%%%%%%%%%%%%%%%%%%%%%%%

wp = cell(1,1024);

for l = 1:1024
    wp{l} = zeros(4,4);  
end


for l = 2:1024
    for i = 1:4
        for j = 1:4
          if  round(new_c{l-1}(i,j)/(ratio*Q8(i,j)))*(ratio*Q8(i,j)) <= round(new_c{l}(i,j)/(ratio*Q8(i,j)))*(ratio*Q8(i,j))
              wp{l}(i,j) = 1;
          else 
              wp{l}(i,j) = 0;
          end
        end
    end
end


IDoubleExtractWater = zeros(128,128);
index = 1;
for i = 1:4:128
    for j = 1:4:128
    IDoubleExtractWater(i:i+3,j:j+3) = wp{index};
    index = index + 1;
    end
end

IExtractWater = 255*uint8(IDoubleExtractWater);

imwrite(IExtractWater,'retrieved_encrypted_watermark.tiff');








                        
